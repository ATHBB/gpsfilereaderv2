﻿using GPSFileReader.Calc;
using GPSFileReader.Data.GPSModels;
using GPSFileReader.FileReader;
using GPSFileReader.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
namespace GPSFileReader
{
    public class GPSFileReader
    {
        List<Waypoint> _tempList;
        readonly IGPSFileReader _fileReader;
        readonly IDistance _distanceFunctions;
        readonly ITime _timeFunctions;
        readonly ISpeed _speedFuctions;
        readonly IElevation _elevationFuctions;
        public readonly MainObject _resultObject;
        readonly ILogger _logger;

        public GPSFileReader(MainObject resultObject, IGPSFileReader fileReader, IDistance distanceFunctions, ITime timeFunctions, ISpeed speedFuctions, IElevation elevationFuctions, ILogger logger)
        {
            _fileReader = fileReader;
            _resultObject = resultObject;
            _distanceFunctions = distanceFunctions;
            _timeFunctions = timeFunctions;
            _speedFuctions = speedFuctions;
            _elevationFuctions = elevationFuctions;
            _logger = logger;
        }

        public void ReadFile(string filePath)
        {
            try
            {
                var gpxPoints = _fileReader.GetDataFromFile(filePath).ToList();
                _tempList = Enumerable.Range(1, gpxPoints.Count() - 1).Select(x =>
                    new Waypoint
                    {
                        Distance = _distanceFunctions.GetDistance(gpxPoints[x - 1], gpxPoints[x]),
                        Time = _timeFunctions.GetTime(gpxPoints[x - 1].Time, gpxPoints[x].Time),
                        ElevationDiff = _elevationFuctions.GetElevation(gpxPoints[x - 1].Ele, gpxPoints[x].Ele),
                        Elevation = gpxPoints[x].Ele
                    }).ToList();
                _tempList.ForEach(x =>
                {
                    x.Speed = _speedFuctions.GetSpeed(x.Distance, x.Time);
                });
                _tempList.Add(new Waypoint
                {
                    Distance = 0d,
                    Time = 0d,
                    Speed = 0d,
                    ElevationDiff = 0d,
                    Elevation = gpxPoints[0].Ele
                });
            }
            catch (Exception ex)
            {
                _logger.AddLogError("", ex);
            }
        }

        public void InitalizeWholeObject()
        {
            if (_tempList.Any())
            {
                _resultObject.Total.SetDataObject(_tempList);
                _resultObject.Climbing.SetDataObject(_tempList.Where(x => x.ElevationDiff > 0).ToList());
                _resultObject.Flat.SetDataObject(_tempList.Where(x => x.ElevationDiff.Equals(0)).ToList());
                _resultObject.Descent.SetDataObject(_tempList.Where(x => x.ElevationDiff < 0).ToList());
                _resultObject.Total.InitializeAll();
                _resultObject.Climbing.InitializeAll();
                _resultObject.Descent.InitializeAll();
                _resultObject.Flat.InitializeAll();
            }
        }
    }
}
