﻿using GPSFileReader.Data.GPSModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPSFileReader.PointsComparer.Successors
{
    public abstract class BaseSuccessor
    {
        protected BaseSuccessor _successor;

        public void SetSuccessor(BaseSuccessor successor)
        {
            _successor = successor;
        }

        public abstract void ComparePoints(GPXModel previousPoint, GPXModel currentPoint);
        
    }
}
