﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GPSFileReader.Data.GPSModels;

namespace GPSFileReader.PointsComparer.Successors
{
    public class FlatSuccessor : BaseSuccessor
    {
        public override void ComparePoints(GPXModel previousPoint, GPXModel currentPoint)
        {
            if (previousPoint.Ele.Equals(currentPoint.Ele))
            {

            }
            else
                _successor.ComparePoints(previousPoint, currentPoint);  
    }
}
