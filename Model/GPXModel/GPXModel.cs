﻿using System;

namespace Model.GPXModel
{
    public class GPXModel
    {
        public double Lon { get; set; }
        public double  Lat { get; set; }
        public double ele { get; set; }
        public DateTime time { get; set; }
    }
}
