﻿using GPSFileReader.Data.GPSModels;
using System.Collections.Generic;

namespace GPSFileReader.Data.DataInitializer
{
    public class BaseDataInitializer 
    {
        List<Waypoint> data;

        public List<Waypoint> Data
        {
            get
            {
                return data;
            }
        }

        public void SetData(List<Waypoint> newData)
        {
            data = newData;
        }
    }
}
