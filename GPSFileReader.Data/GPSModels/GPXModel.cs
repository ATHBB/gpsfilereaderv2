﻿using System;

namespace GPSFileReader.Data.GPSModels
{
    public class GPXModel
    {
        public double Lon { get; set; }
        public double Lat { get; set; }
        public double Ele { get; set; }
        public DateTime Time { get; set; }
    }
}
