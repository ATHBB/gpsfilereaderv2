﻿using GPSFileReader.Data.ResultModels;

namespace GPSFileReader.Data.GPSModels
{
    public  class MainObject
    {
        public ResultObject Climbing { get; set; }
        public ResultObject Descent { get; set; }
        public ResultObject Flat { get; set; }
        public ResultObject Total { get; set; }

        public MainObject(ResultObject climbing, ResultObject descent, ResultObject flat, ResultObject total)
        {
            Climbing = climbing;
            Descent = descent;
            Flat = flat;
            Total = total;
        }
    }
}
