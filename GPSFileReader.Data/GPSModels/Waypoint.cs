﻿namespace GPSFileReader.Data.GPSModels
{
    public class Waypoint
    {
        public double Distance { get; set; }
        public double Speed { get; set; }
        public double ElevationDiff { get; set; }
        public double Time { get; set; }
        public double Elevation { get; set; }
    }
}
