﻿using GPSFileReader.Data.DataInitializer;
using System.Linq;

namespace GPSFileReader.Data.ResultModels
{
    public class Elevation :BaseDataInitializer
    {
        public double FinalBalance { get; set; }
        public double MinElevation { get; set; }
        public double MaxElevation { get; set; }
        public double AverageElevation { get; set; }

        public void CalcFinalBalance()
        {
            FinalBalance = Data[Data.Count - 2].Elevation - Data[0].Elevation;
        }

        public void CalcMinElevation()
        {
            if (Data.Any())
                MinElevation = Data.Min(x => x.Elevation);
        }

        public void CalcMaxElevation()
        {
            if (Data.Any())
                MaxElevation = Data.Max(x => x.Elevation);
        }

        public void CalcAverageElevation()
        {
            if (Data.Any())
                AverageElevation = (Data.Max(x => x.Elevation) + Data.Min(x => x.Elevation)) /2;
        }
    }
}
