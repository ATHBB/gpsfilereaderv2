﻿using GPSFileReader.Data.DataInitializer;
using System;
using System.Linq;

namespace GPSFileReader.Data.ResultModels
{
    public class Time: BaseDataInitializer
    {
        public double TimeInSeconds { get; set; }
        public double TimeInMinutes { get; set; }
        public string TimeInStringFormat { get; set; }

        public void CalcTimeInSeconds()
        {
            if (Data.Any())
                TimeInSeconds = Data.Sum(x => x.Time);
        }

        public void CalcTimeInMinutes()
        {
            if (Data.Any())
                TimeInMinutes = TimeSpan.FromSeconds(Data.Sum(x => x.Time)).Minutes;
        }

        public void SetTimeInStringFormat()
        {
            TimeInStringFormat = TimeSpan.FromSeconds(Data.Sum(x => x.Time)).ToString(@"d' , 'hh\:mm\:ss");
        }
    }
}
