﻿using GPSFileReader.Data.GPSModels;
using System.Collections.Generic;

namespace GPSFileReader.Data.ResultModels
{
    public class ResultObject
    {
        public Distance Distance { get; set; }
        public Elevation Elevation { get; set; }
        public Speed Speed { get; set; }
        public Time Time { get; set; }

        public ResultObject(Speed speed, Elevation elevation, Time time, Distance distance)
        {
            Distance = distance;
            Elevation = elevation;
            Speed = speed;
            Time = time;
        }

        public void InitializeAll()
        {
            InitializeDistance();
            InitializeElevation();
            InitializeSpeed();
            InitializeTime();
        }

        public void InitializeSpeed()
        {
            Speed.CalcMinSpeedInMetersPerSecond();
            Speed.CalcMaxSpeedInMetersPerSecond();
            Speed.CalcAverageSpeedInMetersPerSecond();
            Speed.CalcMinSpeedInKilometersPerHour();
            Speed.CalcMaxSpeedInKilometersPerHour();
            Speed.CalcAverageSpeedInKilometersPerHour();
        }

        public void InitializeDistance()
        {
            Distance.CalcDistanceInMeters();
            Distance.CalcDistanceInKilometers();
        }

        public void InitializeTime()
        {
            Time.CalcTimeInMinutes();
            Time.CalcTimeInSeconds();
            Time.SetTimeInStringFormat();
        }

        public void InitializeElevation()
        {
            Elevation.CalcFinalBalance();
            Elevation.CalcMinElevation();
            Elevation.CalcMaxElevation();
            Elevation.CalcAverageElevation();
        }

        public void SetDataObject(List<Waypoint> data)
        {
            Distance.SetData(data);
            Elevation.SetData(data);
            Time.SetData(data);
            Speed.SetData(data);
        }
    }
}
