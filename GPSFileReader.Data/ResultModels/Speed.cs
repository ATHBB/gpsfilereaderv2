﻿using GPSFileReader.Data.DataInitializer;
using System.Linq;

namespace GPSFileReader.Data.ResultModels
{
    public class Speed : BaseDataInitializer
    {
        public double MinSpeedInMetersPerSecond { get; set; }
        public double MaxSpeedInMetersPerSecond { get; set; }
        public double AverageSpeedInMetersPerSecond { get; set; }
        public double MinSpeedInKilometersPerHour{ get; set; }
        public double MaxSpeedInKilometersPerHour { get; set; }
        public double AverageSpeedInKilometersPerHour { get; set; }

        public void CalcMinSpeedInMetersPerSecond()
        {
            if (Data.Any())
                MinSpeedInMetersPerSecond = Data.Where(x=> !x.Speed.Equals(0)).Min(x => x.Speed);
        }

        public void CalcMaxSpeedInMetersPerSecond()
        {
            if (Data.Any())
                MaxSpeedInMetersPerSecond = Data.Max(x => x.Speed);
        }

        public void CalcAverageSpeedInMetersPerSecond()
        {
            if (Data.Any())
                AverageSpeedInMetersPerSecond = Data.Sum(x => x.Speed)/Data.Sum(x => x.Time);
        }

        public void CalcMinSpeedInKilometersPerHour()
        {
            if (Data.Any())
                MinSpeedInKilometersPerHour = Data.Where(x => x.Speed != 0).Min(x => x.Speed) / 10 * 36;
        }

        public void CalcMaxSpeedInKilometersPerHour()
        {
            if (Data.Any())
                MaxSpeedInKilometersPerHour = Data.Max(x => x.Speed) / 10 * 36;
        }

        public void CalcAverageSpeedInKilometersPerHour()
        {
            if (Data.Any())
                AverageSpeedInKilometersPerHour = Data.Sum(x => x.Speed) / Data.Sum(x => x.Time) / 10 * 36;
        }
    }
}
