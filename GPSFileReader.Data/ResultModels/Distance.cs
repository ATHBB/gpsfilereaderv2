﻿using GPSFileReader.Data.DataInitializer;
using System.Linq;

namespace GPSFileReader.Data.ResultModels
{
    public class Distance  : BaseDataInitializer
    {
        public double DistanceInMeters { get; set; }

        public double DistanceInKilometers { get; set; }

        public void CalcDistanceInMeters()
        {
            if (Data.Any())
                DistanceInMeters = Data.Sum(x => x.Distance);
        }

        public void CalcDistanceInKilometers()
        {
            if (Data.Any())
                DistanceInKilometers = Data.Sum(x => x.Distance) /1000;
        }
    }
}
