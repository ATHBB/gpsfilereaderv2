﻿namespace GPSFileReader.Calc
{
    public interface IElevation
    {
        double GetElevation(double previousElvation, double currentElevation);
    }
}