﻿namespace GPSFileReader.Calc
{
    public interface ISpeed
    {
        double GetSpeed(double distanceInKilometers, double timeInHours);
    }
}