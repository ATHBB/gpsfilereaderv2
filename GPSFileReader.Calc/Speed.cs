﻿namespace GPSFileReader.Calc
{
    public class Speed : ISpeed
    {
        public double GetSpeed(double distanceInKilometers, double timeInHours)
        {
            return distanceInKilometers / timeInHours;
        }
    }
}
