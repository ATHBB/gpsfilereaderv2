﻿using GPSFileReader.Data.GPSModels;

namespace GPSFileReader.Calc
{
    public interface IDistance
    {
        double GetDistance(GPXModel previousWaypoint, GPXModel currentWaypoint);
    }
}