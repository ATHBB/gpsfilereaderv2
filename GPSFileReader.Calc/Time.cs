﻿using System;

namespace GPSFileReader.Calc
{
    public  class Time : ITime
    {
        public double GetTime(DateTime previousTime, DateTime currentTime)
        {
            return (currentTime - previousTime).TotalSeconds;
        }
    }
}
