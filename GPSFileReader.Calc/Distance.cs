﻿using GPSFileReader.Data.GPSModels;
using System;

namespace GPSFileReader.Calc
{
    public class Distance : IDistance
    {
        const double EARTH_RADIUS_KM = 6371;

        double GetSphericalTriangleALength(double latitudeDiff, double longtitudeDiff, double previousLatitude, double currentLatitude)
        {
            return Math.Pow(Math.Sin(ConvertToRadians(latitudeDiff) / 2), 2) + Math.Cos(ConvertToRadians(previousLatitude)) * Math.Cos(ConvertToRadians(currentLatitude)) * Math.Pow(Math.Sin(ConvertToRadians(longtitudeDiff) / 2), 2);
        }

        double GetSphericalTriangleCLength(double a)
        {
            return 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
        }

        double GetDifferenceBetweenPoints(double point1, double point2)
        {
            return point1 - point2;
        }

        public double GetDistance(GPXModel previousWaypoint, GPXModel currentWaypoint)
        {
            return ConvertToMeters(EARTH_RADIUS_KM * GetSphericalTriangleCLength(GetSphericalTriangleALength(GetDifferenceBetweenPoints(currentWaypoint.Lat, previousWaypoint.Lat), GetDifferenceBetweenPoints(currentWaypoint.Lon, previousWaypoint.Lon), previousWaypoint.Lat, currentWaypoint.Lat)));
        }

        double ConvertToMeters(double value)
        {
            return value * 1000;
        }

        double ConvertToRadians(double value)
        {
            return value * (Math.PI / 180);
        }
    }
}
