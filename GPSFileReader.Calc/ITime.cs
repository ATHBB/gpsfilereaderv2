﻿using System;

namespace GPSFileReader.Calc
{
    public interface ITime
    {
        double GetTime(DateTime previousTime, DateTime currentTime);
    }
}