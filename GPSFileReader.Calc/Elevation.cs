﻿namespace GPSFileReader.Calc
{
    public class Elevation : IElevation
    {
        public double GetElevation(double previousElvation, double currentElevation)
        {
            return currentElevation - previousElvation;
        }
    }
}
