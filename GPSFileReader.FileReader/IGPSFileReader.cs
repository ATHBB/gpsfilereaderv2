﻿using System.Collections.Generic;
using GPSFileReader.Data.GPSModels;

namespace GPSFileReader.FileReader
{
    public interface IGPSFileReader
    {
        IEnumerable<GPXModel> GetDataFromFile(string filePath);
    }
}