﻿using GPSFileReader.Logger;
using GPSFileReader.Data.GPSModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace GPSFileReader.FileReader
{
    public class GPSFileReader : IGPSFileReader
    {
        public IEnumerable<GPXModel> GetDataFromFile(string filePath)
        {
            if (File.Exists(filePath))
            {
                return XDocument.Load(filePath).Descendants(XNamespace.Get("http://www.topografix.com/GPX/1/1") + "trkpt").Select(x =>
                     new GPXModel
                     {
                         Ele = double.Parse(x.Descendants(XNamespace.Get("http://www.topografix.com/GPX/1/1") + "ele").FirstOrDefault().Value, System.Globalization.CultureInfo.InvariantCulture),
                         Lat = double.Parse(x.Attribute("lat").Value, System.Globalization.CultureInfo.InvariantCulture),
                         Lon =double.Parse(x.Attribute("lon").Value, System.Globalization.CultureInfo.InvariantCulture),
                         Time = DateTime.ParseExact(x.Descendants(XNamespace.Get("http://www.topografix.com/GPX/1/1") + "time").FirstOrDefault().Value, "yyyy-MM-dd'T'HH:mm:ss'Z'", null, System.Globalization.DateTimeStyles.None)
                     });
            }
            throw new Exception($"File: {filePath} doesn't exsist!");
        }
    }
}
