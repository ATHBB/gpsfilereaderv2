﻿using GPSFileReader.Calc;
using GPSFileReader.Data.GPSModels;
using GPSFileReader.Logger;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Debug
{
    class Program
    {
        static void Main(string[] args)
        {
            var logger = new Logger();
            IDistance distane = new Distance();
            ITime time = new Time();
            ISpeed speed = new Speed();
            IElevation elevation = new Elevation();

            GPSFileReader.GPSFileReader gps = new GPSFileReader.GPSFileReader(
                new MainObject(new GPSFileReader.Data.ResultModels.ResultObject(new GPSFileReader.Data.ResultModels.Speed(), new GPSFileReader.Data.ResultModels.Elevation(), new GPSFileReader.Data.ResultModels.Time(), new GPSFileReader.Data.ResultModels.Distance()),
                new GPSFileReader.Data.ResultModels.ResultObject(new GPSFileReader.Data.ResultModels.Speed(), new GPSFileReader.Data.ResultModels.Elevation(), new GPSFileReader.Data.ResultModels.Time(), new GPSFileReader.Data.ResultModels.Distance()),
                new GPSFileReader.Data.ResultModels.ResultObject(new GPSFileReader.Data.ResultModels.Speed(), new GPSFileReader.Data.ResultModels.Elevation(), new GPSFileReader.Data.ResultModels.Time(), new GPSFileReader.Data.ResultModels.Distance()),
                new GPSFileReader.Data.ResultModels.ResultObject(new GPSFileReader.Data.ResultModels.Speed(), new GPSFileReader.Data.ResultModels.Elevation(), new GPSFileReader.Data.ResultModels.Time(), new GPSFileReader.Data.ResultModels.Distance())),
                new GPSFileReader.FileReader.GPSFileReader(), new Distance(), new Time(), new Speed(), new Elevation(), new Logger());
            gps.ReadFile(ConfigurationManager.AppSettings["GPXFilePath"]);
            gps.InitalizeWholeObject();


            Console.ReadKey();    
        }        
    }
}
